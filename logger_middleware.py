from flask import request
from datetime import datetime

def log_request():
    print(f"Log: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {request.method} {request.url}")
    

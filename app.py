from dotenv import load_dotenv
load_dotenv()
# Description: Fichier principal de l'application Flask
from flask import Flask, jsonify, request
# importation de la classe MySQL depuis le package flask_mysqldb
from flask_mysqldb import MySQL
# importation de la classe CORS depuis le package flask_cors
from flask_cors import CORS
from flask_bcrypt import Bcrypt




# création de l'application Flask
app = Flask(__name__)
bcrypt = Bcrypt(app)
# configuration de la base de données
CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})
app.config.from_object("config")

mysql = MySQL(app)

#importation du middleware
from logger_middleware import log_request
# initialisation du middleware pour logger les requêtes
app.before_request(log_request)

# importation des blueprints après que la base de données ait été configurée
from cars_controller import cars_controller
from garages_controller import garages_controller


# utilisations des blueprints pour gérer les routes
app.register_blueprint(cars_controller, url_prefix='/cars')
app.register_blueprint(garages_controller, url_prefix='/garages')


# route pour obtenir la liste des voitures
@app.route('/cars', methods=['GET'])
def get_cars():
    # connexion à la base de données
    cur = mysql.connection.cursor()
    # exécution de la requête
    cur.execute('SELECT * FROM car')
    # récupération des données
    cars = cur.fetchall()
    # fermeture de la connexion
    cur.close()
    # transformation des données en JSON
    return jsonify(cars)

@app.route('/cars/brands/<string:brand_name>', methods=['GET'])
def get_cars_brand(brand_name):
   cur = mysql.connection.cursor()
   cur.execute('SELECT * FROM car WHERE brand = %s', (brand_name,))
   cars = cur.fetchall()
   cur.close()
   if cars:
     return jsonify(cars)
   else:
     return 'Brand not found', 404


# route pour obtenir une voiture par son id
@app.route('/cars/<int:id>', methods=['GET'])
def get_car(id):
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM car WHERE car_id = %s', (id,))
    cars = cursor.fetchall()
    cursor.close()
    if cars:
        return jsonify(cars)
    else:
        return 'Car not found', 404


# route pour ajouter une voiture    
@app.route('/cars', methods=['POST'])
def add_car():
        # récupération des données envoyées
        data = request.get_json()
        brand = data ['brand']
        model = data['model']
        # connexion à la base de données
        cursor = mysql.connection.cursor()
        # exécution de la requête
        cursor.execute('INSERT INTO car (brand, model) VALUES (%s, %s)', (brand, model))
        # validation de la requête
        mysql.connection.commit()
        # fermeture de la connexion
        cursor.close()

        return jsonify('Car created', 201)

# route pour modifier une voiture par son id
@app.route('/cars/<int:id>', methods=['PUT'])
def update_car(id):
    data = request.get_json()
    brand = data ['brand']
    model = data['model']
    cursor = mysql.connection.cursor()
    cursor.execute('UPDATE car SET brand = %s, model = %s WHERE car_id = %s', (brand, model, id))
    mysql.connection.commit()
    cursor.close()

    return 'Car updated, 200'

# route pour supprimer une voiture par son id
@app.route('/cars/<int:id>', methods=['DELETE'])
def delete_car(id):
    cursor = mysql.connection.cursor()
    cursor.execute('DELETE FROM car WHERE car_id = %s', (id,))
    mysql.connection.commit()
    cursor.close()


    return 'Car deleted', 200    

# route pour creer un compte
@app.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if (email is None or password is None):
        return jsonify({'error': 'please specify both email and password'}),401
    
    # hachage du mot de passe
    hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
    # connexion à la base de données
    cursor = mysql.connection.cursor()
    # execution de la requête sql sur le cursor pour enregistrer le compte dans la base de données
    cursor.execute('INSERT INTO user (email, password) VALUES (%s, %s)', (email, hashed_password))
    # validation de la requête
    mysql.connection.commit()
    user_id = cursor.lastrowid
    # on retourne l'id et l'email du user 
    user_id = {
        'user_id': user_id,
        'email': email
   }
    return jsonify('User_inserted', 201)

#--------------------------------------------------

# route pour verifier si le compte existe
@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if (email is None or password is None):
        return jsonify({'error': 'please specify both email and password'}), 401
     # verifier si le compte existe avec l'email
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM user WHERE email = %s', (email,))
    user = cursor.fetchone()

    if (user is None):
        return jsonify({'error': 'bad email'}), 401
    
    # recuperer le mot de passe haché    
    hashed_password = user['password'] if user else None
    if hashed_password is None:
        return jsonify({'error': 'password not found in database'}), 500
    #comparer avec bcrypt le mot de passe haché et le mot de passe fourni par l'utilisateur
    if (not bcrypt.check_password_hash(hashed_password, password)):
        return jsonify({'error': 'Invalid password'}), 401

    # on retourne l'id et l'email du user
    return jsonify({
        'id': user['user_id'],
        'email': user['email'],
    }), 200








    

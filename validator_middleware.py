import re
from flask import jsonify, request
from functools import wraps

# fonction pour valider les données de la table garage
def validate_garage_data(next):    
    @wraps(next)    
    def wrapper(*args, **kwargs):
        data = request.json
        email = data.get('email')
        garage = data.get('garage')

        # verifie que l'email est valide
        email_regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$' 
        if not re.match(email_regex, email):
        # retourne une erreur 400 si l'email est invalide
            return jsonify({"message":'Invalid email'}), 400
        
        # verifie que les données de la table garage sont fournies
        if not data:
        # retourne une erreur 400 si les données de la table garage sont manquantes    
            return jsonify({"message":'garage data is missing'}), 400
        

        # verifie que l'email et le garage sont présents dans la requête
        if not email or not garage:
        # retourne une erreur 400 si l'email ou le garage sont manquants    
            return jsonify({'message': 'Le titre et l\'auteur sont obligatoires.'}), 400
        else:
        # sinon, continue la requête    
            return next(*args, **kwargs)
        
    return wrapper
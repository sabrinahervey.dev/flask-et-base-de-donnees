from flask import Blueprint, jsonify, request
from app import mysql

cars_controller = Blueprint('cars', __name__)

@cars_controller.route('/cars', methods=['GET'])
def get_cars():
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM cars')
    cars = cursor.fetchall()
    cursor.close()
    return jsonify(cars)

@cars_controller.route('/<int:id>' , methods=['GET'])
def get_car(id):
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM cars WHERE id = %s', (id,))
    cars = cursor.fetchall()
    if cars:
        return jsonify(cars)
    else:
        return 'Car not found', 404
    
@cars_controller.route('/', methods=['POST'])
def created_car():
    data = request.get_json()
    brand = data['brand']
    model = data['model']
    cursor = mysql.connection.cursor()
    cursor.execute('INSERT INTO cars (brand, model) VALUES (%s, %s)', (brand, model))
    mysql.connection.commit()
    cursor.close()
    return jsonify('Car created', 201) 

@cars_controller.route('/<int:id>', methods=['PUT'])
def update_car(id):
    data = request.get_json()
    brand = data['brand']
    model = data['model']
    cursor = mysql.connection.cursor()
    cursor.execute('UPDATE cars SET brand = %s, model = %s WHERE id = %s', (brand, model, id))
    mysql.connection.commit()
    cursor.close()
    return jsonify('Car updated', 200)

@cars_controller.route('/<int:id>', methods=['DELETE'])
def delete_car(id):
    cursor = mysql.connection.cursor()
    cursor.execute('DELETE FROM cars WHERE id = %s', (id,))
    mysql.connection.commit()
    cursor.close()
    return jsonify('Car deleted', 200)

from flask import Blueprint, jsonify, request
from app import mysql
from validator_middleware import validate_garage_data

garages_controller = Blueprint('garages', __name__)

# route pour obtenir la liste des garages
@garages_controller.route('/', methods=['GET'])
def get_garages():
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM garage')
    garages = cursor.fetchall()
    cursor.close()
    return jsonify(garages)

# route pour supprimer un garage
@garages_controller.route('/<int:id>', methods=['DELETE'])
def delete_garages(id):
    cursor = mysql.connection.cursor()
    cursor.execute('DELETE FROM garages WHERE id = %s', (id,))
    mysql.connection.commit()
    cursor.close()
    return jsonify('Garage deleted', 200) 

# route pour creer un garage
@garages_controller.route('/', methods=['POST'])
@validate_garage_data
def created_garage():
    data = request.get_json()
    name = data['name']
    email = data['email']
    cursor = mysql.connection.cursor()
    cursor.execute('INSERT INTO garage (name, email) VALUES (%s, %s)', (name, email))
    mysql.connection.commit()
    cursor.close()
    return jsonify('Garage created', 201)

#route pour modifier un garage
@garages_controller.route('/<int:id>', methods=['PUT'])
@validate_garage_data
def update_garage(id):
    data = request.get_json()
    name = data['name']
    email = data['email']
    cursor = mysql.connection.cursor()
    cursor.execute('UPDATE garage SET name = %s, email = %s WHERE id = %s', (name, email, id))
    mysql.connection.commit()
    cursor.close()
    return jsonify('Garage updated', 200)